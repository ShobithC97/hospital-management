from celery import Celery

import sys, os, django
sys.path.append("/home/shobith/Desktop/hospital_management/hospital_management") #here store is root folder(means parent).
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hospital_management.settings")
django.setup()

from hospital_management import settings
from django.core.mail import EmailMessage
from django.template import loader, Context
from hospital.models import *


app = Celery('tasks', broker='pyamqp://guest@localhost//')

@app.task
def send_appointment_booking_email(patient_id, doctor_id):
    patient = Patient.objects.get(id=patient_id)
    doctor = Doctor.objects.get(id=doctor_id)
    email_from = settings.EMAIL_HOST_USER
    subject="Appointment Booking"
    emailhtml = loader.get_template('email/appointment_booking.html')
    context = {'doctor':doctor.user.full_name, 'patient':patient.user.full_name}
    html_content = emailhtml.render(context)
    msg = EmailMessage(subject,html_content,email_from,[patient.user.email, doctor.user.email])
    msg.content_subtype = "html"
    msg.send()
