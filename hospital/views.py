from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.utils import timezone
import datetime
from .models import *
from tasks import send_appointment_booking_email

# Create your views here.


class Index(ListView):
    template_name = 'hospital/index.html'
    model = Doctor

    def get_queryset(self):
        return Doctor.objects.all()


class MedUserCreationForm(UserCreationForm):
    class Meta:
        model = MedUser
        fields = UserCreationForm.Meta.fields


class SignUp(CreateView):
    form_class = MedUserCreationForm
    template_name = 'registration/signup.html'
    success_url = reverse_lazy('login')


def profile(request):
    if request.user.role == 'PT':
        patient = Patient.objects.get_or_create(
            user=MedUser.objects.get(id=request.user.id))
        appointments = Appointment.objects.filter(patient=patient[0],
                appointment_date__gte=timezone.now()).order_by('appointment_date')
        patient = patient[0]
        context = {'patient': patient, 'appointments': appointments}
        template_name = 'hospital/patientprofile.html'
    elif(request.user.role == 'DR'):
        doctor = Doctor.objects.get_or_create(
            user=MedUser.objects.get(id=request.user.id))
        forenoon_booking = Appointment.objects.filter(doctor=doctor[0],
                appointment_date=timezone.now(),
                time_slot=Appointment.morning_slot).order_by('appointment_date')
        afternoon_booking = Appointment.objects.filter(doctor=doctor[0],
                appointment_date=timezone.now(),
                time_slot=Appointment.evening_slot).order_by('appointment_date')
        upcoming = Appointment.objects.filter(doctor=doctor[0],
                appointment_date__gt=timezone.now()).order_by('appointment_date')
        doctor = doctor[0]
        context = {'doctor': doctor, 'upcoming': upcoming, 'forenoon_booking': forenoon_booking, 'afternoon_booking': afternoon_booking}
        template_name = 'hospital/doctorprofile.html'
    elif(request.user.role == 'AD'):
        return HttpResponseRedirect('/admin/')
        # admin = MedUser.objects.get(id=request.user.id)
        # context = {'admin': admin}
        # template_name = 'hospital/adminprofile.html'
    else:
        return HttpResponse('admin profile')

    return render(request, template_name, context)


class UserForm(forms.ModelForm):
    class Meta:
        model = MedUser
        fields = ['first_name', 'last_name', 'email']


class DoctorForm(forms.ModelForm):
    class Meta:
        model = Doctor
        fields = ['specialization', 'photo']


class PatientForm(forms.ModelForm):
    class Meta:
        model = Patient
        fields = ['photo', 'date_of_birth', 'address']


def edit_profile(request):
    user = MedUser.objects.get(id=request.user.id)
    if request.user.role == 'PT':
        template_name = 'hospital/patienteditprofile.html'
        patient = Patient.objects.get(
            user=MedUser.objects.get(id=request.user.id))
    else:
        template_name = 'hospital/doctoreditprofile.html'
        doctor = Doctor.objects.get(
            user=MedUser.objects.get(id=request.user.id))
    if request.method == 'POST':
        if request.user.role == 'PT':
            form = PatientForm(request.POST, request.FILES, instance=patient)
            userform = UserForm(request.POST, instance=user)
            form.save()
            userform.save()

        if request.user.role=='DR':
            form = DoctorForm(request.POST, request.FILES, instance=doctor)
            userform = UserForm(request.POST, instance=user)
            print(request.FILES)
            form.save()
            userform.save()

        return HttpResponseRedirect('/profile/')

    else:
        userform = UserForm(instance=user)
        if(request.user.role == 'PT'):
            form = PatientForm(instance=patient)

        if(request.user.role == 'DR'):
            form = DoctorForm(instance=doctor)

        context = {'form': form, 'userform': userform}

        return render(request, template_name, context)

class ChooseDoctor(ListView):
    template_name = 'hospital/choosedoctor.html'
    model = Doctor

    def get_queryset(self):
        return Doctor.objects.all()

def get_token(date,doctor):
    appointments = Appointment.objects.filter(appointment_date=date,
                                              doctor=doctor).order_by('-id')
    if appointments:
        return appointments[0].token+1
    else:
        return 1


class AddAppointmentForm(forms.ModelForm):
    class Meta:
        model = Appointment
        fields = ['time_slot', 'appointment_date']

class CreateAppointment(CreateView):
    model = Appointment
    form_class = AddAppointmentForm
    template_name = 'hospital/appointment.html'
    success_url = reverse_lazy('profile')

    def form_valid(self, form):
        if(self.request.user.is_authenticated):
            patient = Patient.objects.get(user__id=self.request.user.id)
            doctor = Doctor.objects.get(id=self.kwargs.get('doctor_id'))
            self.object = form.save(commit=False)
            self.object.patient = patient
            self.object.doctor = doctor
            self.object.token = get_token(date=self.object.appointment_date,
                                          doctor=self.object.doctor)
            self.object.save()
            send_appointment_booking_email.apply_async([patient.id, doctor.id])
            return super(CreateAppointment, self).form_valid(form)

class AddReportForm(forms.ModelForm):
    class Meta:
        model = Report
        fields = ['doctor','details']

class CreateReport(CreateView):
    model = Report
    form_class = AddReportForm
    template_name = 'hospital/createreport.html'
    success_url = reverse_lazy('profile')

    def form_valid(self, form):
        if(self.request.user.is_authenticated):
            patient = Patient.objects.get(id=self.kwargs.get('patient_id'))
            self.object = form.save(commit=False)
            self.object.patient = patient
            self.object.save()
            return super(CreateReport, self).form_valid(form)

class ViewAllReports(ListView):
    template_name = 'hospital/viewallreports.html'
    model = Report

    def get_queryset(self):
        return Report.objects.filter(patient_id=self.kwargs.get('patient_id'))
