from django.contrib import admin
from .models import *

# Register your models here.
class MedUserAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ('username','password')}),
        ('Name',{'fields':['first_name','last_name']}),
        ('Role', {'fields': ['role']}),
        ('Email',{'fields':['email']}),
    ]
    list_display = ('username', 'email', 'role')
    list_filter=['username']
    ordering=['username']
    search_fields = ['username']

admin.site.register(MedUser,MedUserAdmin)

class PatientAdmin(admin.ModelAdmin):
    fieldsets=[
    (None, {'fields':['user']}),
    ('Details',{'fields':['date_of_birth','address']}),
    ('Photo',{'fields':['photo']})
    ]
    list_filter=['user']
    ordering=['user']
    search_fields = ['user']

admin.site.register(Patient,PatientAdmin)

class DoctorAdmin(admin.ModelAdmin):
    fieldsets=[
    (None, {'fields':['user']}),
    ('Details',{'fields':['specialization']}),
    ('Photo',{'fields':['photo']})
    ]
    list_display = ('user','specialization')
    list_filter=['user']
    ordering=['user']
    search_fields = ['user']

admin.site.register(Doctor,DoctorAdmin)

class AppointmentAdmin(admin.ModelAdmin):
    fieldsets=[
    (None, {'fields':['patient','doctor']}),
    ('Details',{'fields':['time_slot','appointment_date','token']})
    ]
    list_display = ('__str__','token','appointment_date')

admin.site.register(Appointment,AppointmentAdmin)

class ReportAdmin(admin.ModelAdmin):
    fieldsets=[
    (None, {'fields':['patient','doctor']}),
    ('Details',{'fields':['details']})
    ]

admin.site.register(Report,ReportAdmin)
