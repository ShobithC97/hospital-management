from django import template
from django.utils import *
from ..models import *
from datetime import date

# template tags
register = template.Library()

@register.simple_tag
def patient_profile_complete(id):
    patient = Patient.objects.get(id=id)
    values = []
    if patient.photo:
        values.append(True)
    else:
        values.append(False)
    if patient.address:
        values.append(True)
    else:
        values.append(False)
    if patient.date_of_birth:
        values.append(True)
    else:
        values.append(False)
    if patient.user.first_name and patient.user.last_name:
        values.append(True)
    else:
        values.append(False)
    status = False
    for i in values:
        if i == False:
            status = True
    values.append(status)
    return values

@register.simple_tag
def doctor_profile_complete(id):
    doctor = Doctor.objects.get(id=id)
    values = []
    if doctor.photo:
        values.append(True)
    else:
        values.append(False)
    if doctor.specialization:
        values.append(True)
    else:
        values.append(False)
    if doctor.user.first_name and doctor.user.last_name:
        values.append(True)
    else:
        values.append(False)
    return values

@register.simple_tag
def calculate_date(date):
    if date == None:
        return "2018-02-28"
    if date.month < 10:
        month = '0'+str(date.month)
    else:
        month = str(date.month)
    if date.day < 10:
        day = '0'+str(date.day)
    else:
        day = str(date.day)
    return str(date.year)+'-'+month+'-'+day

@register.simple_tag
def count_appointments(id):
    return len(Appointment.objects.filter(patient_id=id, appointment_date=date.today()))

@register.simple_tag
def count_upcoming_appointments(id):
    return len(Appointment.objects.filter(doctor_id=id, appointment_date__gt=date.today()))

@register.simple_tag
def count_forenoon_appointment(id):
    return len(Appointment.objects.filter(doctor_id=id, appointment_date=date.today(), time_slot=Appointment.morning_slot))

@register.simple_tag
def count_afternoon_appointment(id):
    return len(Appointment.objects.filter(doctor_id=id, appointment_date=date.today(), time_slot=Appointment.evening_slot))
