from django.urls import path
from django.contrib.auth import views as auth_views
from .views import *
urlpatterns = [
    path('',Index.as_view(),name='home'),
    path('login/',auth_views.LoginView.as_view(),name='login'),
    path('logout/',auth_views.LogoutView.as_view(),name='logout'),
    path('signup/',SignUp.as_view(),name='signup'),
    path('profile/',profile,name='profile'),
    path('editprofile/',edit_profile,name='editprofile'),
    path('choosedoctor/',ChooseDoctor.as_view(),name='choosedoctor'),
    path('appointment/<int:doctor_id>/',CreateAppointment.as_view(),name='appointment'),
    path('report/<int:patient_id>/',CreateReport.as_view(),name='addreport'),
    path('allreports/<int:patient_id>/',ViewAllReports.as_view(),name='allreports'),
]
