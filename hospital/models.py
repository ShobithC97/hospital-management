from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import date
from django.utils import timezone
# Create your models here.
class MedUser(AbstractUser):
    Admin = 'AD'
    Doctor = 'DR'
    Patient = 'PT'

    ROLES = (
        (Admin,'Admin'),
        (Doctor,'Doctor'),
        (Patient,'Patient'),
        )
    role = models.CharField(max_length=2, choices=ROLES, default=Patient)

    def __str__(self):
        return self.username

    def full_name(self):
        return self.first_name + " " + self.last_name

class Patient(models.Model):
    user = models.ForeignKey(MedUser, on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='patientimage', null=True, blank=True)
    date_of_birth = models.DateField(null=True,blank=True)
    address = models.CharField(null=True,blank=True,max_length=300)

    def __str__(self):
        return self.user.username

    def get_age(self):
        today = date.today()
        return today.year - self.date_of_birth.year - ((today.month, today.day) < (self.date_of_birth.month, self.date_of_birth.day))

class Doctor(models.Model):
    user = models.ForeignKey(MedUser, on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='doctorimage', null=True, blank=True)
    specialization = models.CharField(max_length=100)

    def __str__(self):
        return self.user.username

class Appointment(models.Model):
    morning_slot = 'MS'
    evening_slot = 'ES'
    TIMESLOTS = (
        (morning_slot,'09:00AM-12:00PM'),
        (evening_slot,'01:00PM-04:00PM'),
        )
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    time_slot = models.CharField(max_length=2, choices=TIMESLOTS, default=morning_slot)
    appointment_date = models.DateField(default = timezone.now().date())
    token = models.IntegerField()

    def __str__(self):
        return 'APP'+str(self.id).zfill(4)

class Report(models.Model):
    doctor = models.CharField(max_length=20, default='N/A', null=True, blank=True)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    details = models.TextField(null=True, blank=True)

    def __str__(self):
        return 'REP'+str(self.id).zfill(4)
